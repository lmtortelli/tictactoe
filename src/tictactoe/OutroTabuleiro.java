/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tictactoe;

import java.util.Vector;

/**
 *
 * @author Weslen
 */
public class OutroTabuleiro {
    private char[] tabela;
    private int pai;
    private int numFilhos;
    private int peso;
    private boolean Min;
   
    
    
    OutroTabuleiro(){
        this.pai = -1;
        this.peso = 0;
        this.Min = false;
        this.tabela = new char[9];  
        this.numFilhos = 0;
        
    }
    
    public boolean getMin(){
        return this.Min;
    }
    
    public void calcNumFilhos(){
        
        for(int x = 0; x < 9; x++){
            if(this.tabela[x] == 0){
                this.numFilhos++;                
            }                
        }        
    }
    
    public void calcPesos(int pesoDeEntrada){
        
        this.peso = this.peso + pesoDeEntrada;
    }
    
    public int getNumFilhos() {
        return numFilhos;
    }

    public void setNumFilhos(int numFilhos) {
        this.numFilhos = numFilhos;
    }

    public char[] getTabela() {
        return tabela;
    }

    public void setCharTabela(char jogador, int pos){
        this.tabela[pos] = jogador;
    }
    
    public char getPosTabela(int posicao){
        return this.tabela[posicao];
    }
    
    public void setTabela(char[] tabela) {
        this.tabela = tabela;
    }

    public int getPai() {
        return pai;
    }

    public void setPai(int pai) {
        this.pai = pai;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public boolean isMin() {
        return Min;
    }

    public void setMin(boolean Min) {
        this.Min = Min;
    }
    
    public void clon(OutroTabuleiro entrada){
        for(int x = 0; x < 9; x++){
            this.tabela[x] = entrada.getPosTabela(x);
        }
        
    }
    
    
    
}