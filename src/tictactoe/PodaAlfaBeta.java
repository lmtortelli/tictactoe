/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tictactoe;

import java.util.Random;
import java.util.Vector;

/**
 *
 * @author Weslen
 */
public class PodaAlfaBeta {
    Vector<OutroTabuleiro> arvore;
    boolean jogador; 
    private Interface inter;
    int profundidade;
    
    public PodaAlfaBeta(boolean b){
        this.arvore = new Vector();
        this.jogador = false;
        inter = new Interface();
        inter.setVisible(true);
        inter.setpvc(true);
        inter.setAlphaBeta(b);
    }
    
    public void criaArvore(int profundidade, OutroTabuleiro entrada){
        OutroTabuleiro pai =  new OutroTabuleiro();
        int k = 0;
        int controleDoPai ,  texteSeTemPai,texteSeTemAvo = 0, somador = 0;
        int profundidadeMaxima;
        int contadorDeFilhos;
        int contNumJogadas;
        this.profundidade = profundidade;
        entrada.calcNumFilhos();
        contNumJogadas = entrada.getNumFilhos();
        
         
       profundidadeMaxima = entrada.getNumFilhos();
                 
        
        pai.clon(entrada);
        pai.setMin(entrada.getMin());
        pai.calcNumFilhos();
        
        this.arvore.add(k, pai);
       
       
        while(k < profundidade){
            controleDoPai = 0;
            
            if(k>profundidadeMaxima){
                break;
            }
         
            do{
                contadorDeFilhos = 0;
                while(contadorDeFilhos < this.arvore.get(somador).getNumFilhos()){
                   
                    for(int x = 0; x < 9; x++ ){
                        
                       if(contadorDeFilhos == this.arvore.get(somador).getNumFilhos()){
                           break;
                       }


                       else if((this.arvore.get(somador).getPosTabela(x) == 0) && (this.arvore.get(somador).getNumFilhos() > 0)){
                            OutroTabuleiro filho =  new OutroTabuleiro();
                            contadorDeFilhos++;
                            if(this.arvore.get(somador).isMin()){
                                filho.clon(this.arvore.get(somador));
                                filho.setPai(somador);
                                filho.setCharTabela('O', x);
                                filho.calcNumFilhos();
                                filho.setMin(false);

                                setPesos(filho,k);

                                this.arvore.add(filho);                            
                            }
                            else{
                                filho.clon(this.arvore.get(somador));
                                filho.setPai(somador);
                                filho.setCharTabela('X', x);
                                filho.calcNumFilhos();
                                filho.setMin(true);

                                setPesos(filho,k);

                                this.arvore.add(filho);   

                            }

                        }

                    }
                }
               texteSeTemPai = this.arvore.get(somador).getPai();
               
               
               if(texteSeTemPai != -1){
                    texteSeTemAvo = this.arvore.get(texteSeTemPai).getPai();
                    if(texteSeTemAvo == -1){
                        texteSeTemAvo = 1;
                    }
                    else{
                       texteSeTemAvo = this.arvore.get(texteSeTemAvo).getNumFilhos();
                    }
               }
                controleDoPai++;
                somador++;                         
               
            }while((texteSeTemPai != -1)  && (controleDoPai < (texteSeTemAvo * this.arvore.get(texteSeTemPai).getNumFilhos())) );
            
            if(k==0){
                somador = 1;
            }
            else if (k==1){
                somador = contNumJogadas + 1;
            }
            else if (k==2){
                somador = (contNumJogadas + 1) + ((contNumJogadas * (contNumJogadas - 1))) ;
            }
            else if (k==3){
                somador =  (contNumJogadas + 1) + (((contNumJogadas * (contNumJogadas - 1)) * (contNumJogadas - 2)));
            }
            
            
            k++;            
        }
        
        
    }
    
    public int buscaNodoComMelhorPoda(){
        int posicao = -1;
        int valor = 0;
        int x =  this.arvore.size() - 1;
      int melhorJogada = 0;
       
        for( ; x >= 0; x--){
            
            if(valor < this.arvore.get(x).getPeso()){
                posicao = x;
                valor = this.arvore.get(x).getPeso();
            }
            
        }
        
        if(posicao == -1){
            Random gerador = new Random();
            posicao = gerador.nextInt(this.arvore.size()-1) + 1;
            
              
        }
        
        
        x = 14;
        while(this.arvore.get(posicao).getPai() != 0){
           melhorJogada = this.arvore.get(posicao).getPai() ;
           posicao = melhorJogada;
           x--;
           
        }
        
        
        return posicao;
            
    }
    
    public void setPesos(OutroTabuleiro entrada, int profundidade){
        
        if( (this.jogador) && ((entrada.getPosTabela(0) == 'O') && (entrada.getPosTabela(1) == 'O') && (entrada.getPosTabela(2) == 'O'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }   
        if( (this.jogador) && ((entrada.getPosTabela(3) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(5) == 'O'))){
             entrada.calcPesos(5 + (10-profundidade));
             entrada.setNumFilhos(0);
            
        }
        if( (this.jogador) && ((entrada.getPosTabela(6) == 'O') && (entrada.getPosTabela(7) == 'O') && (entrada.getPosTabela(8) == 'O'))){
             entrada.calcPesos(5 + (10-profundidade));
             entrada.setNumFilhos(0);
            
        }
        if( (this.jogador) && ((entrada.getPosTabela(0) == 'O') && (entrada.getPosTabela(3) == 'O') && (entrada.getPosTabela(6) == 'O'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (this.jogador) && ((entrada.getPosTabela(1) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(7) == 'O'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (this.jogador) && ((entrada.getPosTabela(2) == 'O') && (entrada.getPosTabela(5) == 'O') && (entrada.getPosTabela(8) == 'O'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if((entrada.getPosTabela(0) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(8) == 'O')){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (this.jogador) && ((entrada.getPosTabela(2) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(6) == 'O'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        
        //----------------------------------------------------------------------------------------------------
       
        if( (!this.jogador) && ((entrada.getPosTabela(0) == 'X') && (entrada.getPosTabela(1) == 'X') && (entrada.getPosTabela(2) == 'X'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }   
        if( (!this.jogador) && ((entrada.getPosTabela(3) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(5) == 'X'))){
             entrada.calcPesos(5 + (10-profundidade));
             entrada.setNumFilhos(0);
            
        }
        if( (!this.jogador) && ((entrada.getPosTabela(6) == 'X') && (entrada.getPosTabela(7) == 'X') && (entrada.getPosTabela(8) == 'X'))){
             entrada.calcPesos(5 + (10-profundidade));
             entrada.setNumFilhos(0);
            
        }
        if( (!this.jogador) && ((entrada.getPosTabela(0) == 'X') && (entrada.getPosTabela(3) == 'X') && (entrada.getPosTabela(6) == 'X'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (!this.jogador) && ((entrada.getPosTabela(1) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(7) == 'X'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (!this.jogador) && ((entrada.getPosTabela(2) == 'X') && (entrada.getPosTabela(5) == 'X') && (entrada.getPosTabela(8) == 'X'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (!this.jogador) && ((entrada.getPosTabela(0) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(8) == 'X'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        if( (!this.jogador) && ((entrada.getPosTabela(2) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(6) == 'X'))){
            entrada.calcPesos(5 + (10-profundidade));
            entrada.setNumFilhos(0);
        }
        
        
        
        
    }
    
    public void imprime(int pos){
        OutroTabuleiro copia = new OutroTabuleiro();
        copia = this.arvore.get(pos);
        
        for(int x = 0; x < 9; x++){
            
            if(copia.getPosTabela(x)!=0)
               inter.Log.setText(inter.Log.getText() + copia.getPosTabela(x) + " ");
            else
                inter.Log.setText(inter.Log.getText()+'*'+" ");
            if((x == 2) || (x == 5))
                inter.Log.setText(inter.Log.getText() + "\n");
            
        }
           inter.Log.setText(inter.Log.getText() + "\n\n");
           
        
        
        
    }
    
    public int getProfundidade(){
        return this.profundidade;
    }
    
    public void jogoPCxPC(OutroTabuleiro entrada){
        boolean set = false ;
        int pos;
        int k = 0;
        inter.Log.setText("# Computer vs. Computer #\n\nDepth: " + getProfundidade()+"\n\n");
        inter.ativeClean();
        
        
        
       while(k < 9){
            entrada.setMin(set); 
            this.jogador = set;

            criaArvore(2,entrada);
            pos = buscaNodoComMelhorPoda();
            entrada.clon(this.arvore.get(pos));


           imprime(pos); 
           inter.attTab(arvore.get(pos).getTabela());
           if(vencedor(entrada,set)!=0)
               break;
           
           
           set = !set;
           k++;
           this.arvore.clear();
      
       }
        
        
    }

    public void playerXPC(OutroTabuleiro entrada){
        int pos;
        entrada.setMin(true); 
        this.jogador = true;
        inter.Log.setText("# Player vs. Computer #\n\nDepth: " + getProfundidade()+"\n\n");
        inter.ativeClean();
        
        
        criaArvore(7,entrada);
        pos = buscaNodoComMelhorPoda();
        entrada.clon(this.arvore.get(pos));
        
        entrada.setMin(false); 
        this.jogador = false;
            
        imprime(pos);
        inter.attTab(arvore.get(pos).getTabela());
    }

    public int vencedor(OutroTabuleiro entrada,boolean min){
        
        if( (min) && ((entrada.getPosTabela(0) == 'O') && (entrada.getPosTabela(1) == 'O') && (entrada.getPosTabela(2) == 'O'))){
            System.out.println("O foi o Vencedor!!");
            return 1;
        }   
        if( (min) && ((entrada.getPosTabela(3) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(5) == 'O'))){
             System.out.println("O foi o Vencedor!!");
            return 1;
            
        }
        if( (min) && ((entrada.getPosTabela(6) == 'O') && (entrada.getPosTabela(7) == 'O') && (entrada.getPosTabela(8) == 'O'))){
            System.out.println("O foi o Vencedor!!");
            return 1; 
            
        }
        if( (min) && ((entrada.getPosTabela(0) == 'O') && (entrada.getPosTabela(3) == 'O') && (entrada.getPosTabela(6) == 'O'))){
           System.out.println("O foi o Vencedor!!");
            return 1;
        }
        if( (min) && ((entrada.getPosTabela(1) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(7) == 'O'))){
           System.out.println("O foi o Vencedor!!");
            return 1;
        }
        if( (min) && ((entrada.getPosTabela(2) == 'O') && (entrada.getPosTabela(5) == 'O') && (entrada.getPosTabela(8) == 'O'))){
           System.out.println("O foi o Vencedor!!");
            return 1;
        }
        if((entrada.getPosTabela(0) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(8) == 'O')){
            System.out.println("O foi o Vencedor!!");
            return 1;
        }
        if( (min) && ((entrada.getPosTabela(2) == 'O') && (entrada.getPosTabela(4) == 'O') && (entrada.getPosTabela(6) == 'O'))){
            System.out.println("O foi o Vencedor!!");
            return 1;
        }
        
        //----------------------------------------------------------------------------------------------------
       
        if( (!min) && ((entrada.getPosTabela(0) == 'X') && (entrada.getPosTabela(1) == 'X') && (entrada.getPosTabela(2) == 'X'))){
           System.out.println("X foi o Vencedor!!");
            return 2;
        }   
        if( (!min) && ((entrada.getPosTabela(3) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(5) == 'X'))){
             System.out.println("X foi o Vencedor!!");
            return 2;
            
        }
        if( (!min) && ((entrada.getPosTabela(6) == 'X') && (entrada.getPosTabela(7) == 'X') && (entrada.getPosTabela(8) == 'X'))){
              System.out.println("X foi o Vencedor!!");
            return 2;
            
        }
        if( (!min) && ((entrada.getPosTabela(0) == 'X') && (entrada.getPosTabela(3) == 'X') && (entrada.getPosTabela(6) == 'X'))){
            System.out.println("X foi o Vencedor!!");
            return 2;
        }
        if( (!min) && ((entrada.getPosTabela(1) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(7) == 'X'))){
           System.out.println("X foi o Vencedor!!");
            return 2;
        }
        if( (!min) && ((entrada.getPosTabela(2) == 'X') && (entrada.getPosTabela(5) == 'X') && (entrada.getPosTabela(8) == 'X'))){
            System.out.println("X foi o Vencedor!!");
            return 2;
        }
        if( (!min) && ((entrada.getPosTabela(0) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(8) == 'X'))){
            System.out.println("X foi o Vencedor!!");
            return 2;
        }
        if( (!min) && ((entrada.getPosTabela(2) == 'X') && (entrada.getPosTabela(4) == 'X') && (entrada.getPosTabela(6) == 'X'))){
            System.out.println("X foi o Vencedor!!");
            return 2;
        }
        
        
        
        return 0;
        
    }
}
