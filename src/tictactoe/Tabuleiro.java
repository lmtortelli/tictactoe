/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;
import java.math.*;
/**
 *
 * @author Lucas
 */
public class Tabuleiro {
    protected char[] tabuleiro;
    protected int custo;
    protected Tabuleiro pai;
    
    public Tabuleiro(){
        this.tabuleiro = new char[9];
        
        //INICIALIZA SEM VALORES, O 0 INDICA NULO
        for(int i=0;i<9;i++){
            this.tabuleiro[i] = '0';
        }
        this.pai = null;
        this.custo = 999;
    }
    
    /*public Tabuleiro(char[] novoTabuleiro){
        this.tabuleiro = new char[9];
        
        //INICIALIZA SEM VALORES, O 0 INDICA NULO
        for(int i=0;i<9;i++){
            this.tabuleiro[i] = novoTabuleiro[i];
        }
        this.custo = 999;
    }*/
    
    public Tabuleiro(Tabuleiro pai){
        int i=0;
        this.tabuleiro = new char[9];
        
        //INICIALIZA SEM VALORES, O 0 INDICA NULO
        for(i=0;i<9;i++){
            this.tabuleiro[i] = pai.tabuleiro[i];
        }
        this.pai = pai;
        this.custo = 999;
    }
    
    public Tabuleiro getPaiProfundidade(Tabuleiro estadoResposta, int profundidadeMaxima){
        int aux=1;
        Tabuleiro auxTab;
        auxTab = estadoResposta;
        if(profundidadeMaxima==1){
            return estadoResposta;
        }
        while(aux<=(profundidadeMaxima)){
            //auxTab.imprimeTabuleiro();
            auxTab = auxTab.pai;
            aux++;
        }
        return auxTab;
    }
    
    public boolean checkAnswer(){
        
        if((this.tabuleiro[0] == this.tabuleiro[1])&&(this.tabuleiro[0] == this.tabuleiro[2])&&(this.tabuleiro[0]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[0] == this.tabuleiro[4])&&(this.tabuleiro[0] == this.tabuleiro[8])&&(this.tabuleiro[0]!='0')){
            return true;
        }

        else if((this.tabuleiro[0] == this.tabuleiro[3])&&(this.tabuleiro[0] == this.tabuleiro[6])&&(this.tabuleiro[0]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[1] == this.tabuleiro[4])&&(this.tabuleiro[1] == this.tabuleiro[6])&&(this.tabuleiro[1]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[2] == this.tabuleiro[5])&&(this.tabuleiro[2] == this.tabuleiro[8])&&(this.tabuleiro[2]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[3] == this.tabuleiro[4])&&(this.tabuleiro[3] == this.tabuleiro[5])&&(this.tabuleiro[3]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[6] == this.tabuleiro[7])&&(this.tabuleiro[6] == this.tabuleiro[8])&&(this.tabuleiro[6]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[2] == this.tabuleiro[4])&&(this.tabuleiro[2] == this.tabuleiro[6])&&(this.tabuleiro[2]!='0')){
            return true;
        }
        
        else if((this.tabuleiro[0] == this.tabuleiro[3])&&(this.tabuleiro[0] == this.tabuleiro[6])&&(this.tabuleiro[0]!='0')){
            return true;
        }
        
        return false;
    }
    
    public void alteraEstado(int indice, boolean player1){
        //player 1 sendo true, indica que o X esta jogando
        //Caso seja false, então é o O que modifica o estado
        if(player1==true){
            //VERIFICA SE A POSICAO OBSERVADA, ESTA NULA (SEM VALOR)
            if(this.tabuleiro[indice]=='0'){
                this.tabuleiro[indice] = 'X';
            }
        }
        
        else if (player1==false){
            if(this.tabuleiro[indice]=='0'){
                this.tabuleiro[indice] = 'O';
            }
        }//Caso false
    }
    
    public char[] getTab(){
        return tabuleiro;
    }
    
    public void imprimeTabuleiro(){
           int indice;
           for(indice=0;indice<9;indice++){
               int terceiro=(indice)%3;
               if(terceiro==0){
                   System.out.println();
               }
               if(this.tabuleiro[indice]=='0'){
                    System.out.print(". ");}
               else{
                   System.out.print(this.tabuleiro[indice]+" ");
               }
           }
    }
    
    public boolean verificaCritico(int indice){
        if(indice == 0){
            if((this.tabuleiro[1]==this.tabuleiro[2])&&(this.tabuleiro[1]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[3] == this.tabuleiro[6])&&(this.tabuleiro[3]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[4]==this.tabuleiro[8])&&(this.tabuleiro[4]!='0')){
                return true;
            }
        }
        
        else if(indice==1){
            if((this.tabuleiro[0]==this.tabuleiro[2])&&(this.tabuleiro[0]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[4]==this.tabuleiro[7])&&(this.tabuleiro[4]!='0')){
                return true;
            }
        }
        
        else if(indice==2){
            if((this.tabuleiro[4]==this.tabuleiro[7])&&(this.tabuleiro[4]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[0] == this.tabuleiro[1])&&(this.tabuleiro[0]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[5]==this.tabuleiro[8])&&(this.tabuleiro[5]!='0')){
                return true;
            }
        }
        
        
        else if(indice==3){
            if((this.tabuleiro[0]==this.tabuleiro[6])&&(this.tabuleiro[0]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[4] == this.tabuleiro[5])&&(this.tabuleiro[4]!='0')){
                return true;
            }
            
        }
        
        
        else if(indice==4){
            if((this.tabuleiro[0]==this.tabuleiro[8])&&(this.tabuleiro[0]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[2] == this.tabuleiro[6])&&(this.tabuleiro[2]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[1]==this.tabuleiro[7])&&(this.tabuleiro[7]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[3]==this.tabuleiro[5])&&(this.tabuleiro[3]!='0')){
                return true;
            }
        }
        
        
        else if(indice==5){
            if((this.tabuleiro[3]==this.tabuleiro[4])&&(this.tabuleiro[3]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[2] == this.tabuleiro[8])&&(this.tabuleiro[2]!='0')){
                return true;
            }
            
        }
        
        
        else if(indice==6){
            if((this.tabuleiro[0]==this.tabuleiro[3])&&(this.tabuleiro[0]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[7] == this.tabuleiro[8])&&(this.tabuleiro[7]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[2]==this.tabuleiro[4])&&(this.tabuleiro[2]!='0')){
                return true;
            }
        }
        
        
        else if(indice==7){
            if((this.tabuleiro[6]==this.tabuleiro[8])&&(this.tabuleiro[6]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[1] == this.tabuleiro[4])&&(this.tabuleiro[1]!='0')){
                return true;
            }
            
        }
        
        
        else if(indice==8){
            if((this.tabuleiro[2]==this.tabuleiro[5])&&(this.tabuleiro[2]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[0] == this.tabuleiro[4])&&(this.tabuleiro[0]!='0')){
                return true;
            }
            
            else if((this.tabuleiro[7]==this.tabuleiro[6])&&(this.tabuleiro[7]!='0')){
                return true;
            }
        }
       
        return false;
    }
    
    public void setCusto(){
        int i;
        int custoX=0,custoO=0;
        for(i=0;i<9;i++){
            
            this.custo = 0;
            //DEFINE O CUSTO COM BASE NA HEURISTICA
            if(i==0){
                if(this.tabuleiro[i]=='X'){
                   
                    if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=3;}
                    
               }
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=3;}
                    
                }
            }
            else if(i==1){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=2;}
                }
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=2;}          
                }
            }
            else if(i==2){
                if(this.tabuleiro[i]=='X'){
                    if(this.verificaCritico(i)){
                            custoX+=5;
                        }
                        else{
                            custoX+=3;}

               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=3;}
                }
            }
                   
            else if(i==3){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=2;}
                    
               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=2;}
                    }
            }
            else if(i==4){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=4;}
                    
               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=4;}
                    }
            }
            else if(i==5){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=2;}
                    
               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=2;}
                    }
                   
            }
            else if(i==6){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=3;}
                    
               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=3;}
                    }
                  
            }
            else if(i==7){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=2;}
                    
               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=2;}
                    }
                  
            }
            else if(i==8){
                if(this.tabuleiro[i]=='X'){
                   if(this.verificaCritico(i)){
                        custoX+=5;
                    }
                    else{
                        custoX+=3;}
                    
               } 
                else if(this.tabuleiro[i]=='O'){
                   if(this.verificaCritico(i)){
                        custoO+=5;
                    }
                    else{
                        custoO+=3;}
                    }       
            }    
        }
        this.custo+= custoX-custoO;
    }

    public int getCusto(){
        return this.custo;
    }
    
    public char getValorIndice(int i){
        
        return this.tabuleiro[i];
    }
    
}
